"use strict";

window.addEventListener("DOMContentLoaded", (event) => {
	const counterElement = document.getElementById("counter");
	const targetDate = new Date("2023-07-08T11:00:00Z");

	const canvas = document.getElementById("pointless-picture");
	/** @type {CanvasRenderingContext2D} */
	const ctx = canvas.getContext("2d");
	let animationTimer = null;

	ctx.lineWidth = 5;
	ctx.translate(canvas.getAttribute("width") / 2, canvas.getAttribute("height") / 2);
	ctx.scale(100 / canvas.getAttribute("width"), 100 / canvas.getAttribute("height"));
	ctx.fillStyle = "pink";


	function update() {
		const time = targetDate - Date.now();
		const days = `${(time / (1000 * 60 * 60 * 24)) >> 0}`
		const hours = `${(time / (1000 * 60 * 60) % 24) >> 0}`.padStart(2, "0");
		const minutes = `${(time / (1000 * 60) % 60) >> 0}`.padStart(2, "0");
		const seconds = `${(time / (1000) % 60) >> 0}`.padStart(2, "0");

		counterElement.innerText = `${days} dni, ${hours}:${minutes}:${seconds}`;
	}
	update();
	animate();


	window.setInterval(update, 1000);



	function lineToRotated(cx, cy, radius, angle) {
		const x = cx + radius * Math.sin(angle);
		const y = cy + radius * Math.cos(angle);

		ctx.lineTo(x, y);
	}

	function moveToRotated(cx, cy, radius, angle) {
		const x = cx + radius * Math.sin(angle);
		const y = cy + radius * Math.cos(angle);

		ctx.moveTo(x, y);
	}


	function animate() {
		const time = targetDate - Date.now();

		ctx.clearRect(-50, -50, 100, 100);
		moveToRotated(0, 0, 10, 0.001 * time);

		ctx.beginPath();

		lineToRotated(20, 15, 10, 0.011 * time);
		lineToRotated(-30, -30, 12, 0.003 * time);
		lineToRotated(-20, 15, 20, 0.0011 * time);
		lineToRotated(-5, -5, 30, 0.02 * time);
		lineToRotated(4, -8, 11, 0.005 * time);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();

	}

	canvas.addEventListener("click", () => {
		if (animationTimer) {
			window.clearInterval(animationTimer);
			animationTimer = null;
		} else {
			animationTimer = window.setInterval(animate, 50);
		}
	});
});
